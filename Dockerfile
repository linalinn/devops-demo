FROM python:3

COPY main.py .

EXPOSE 80:80

ENTRYPOINT /usr/local/bin/python3 /main.py