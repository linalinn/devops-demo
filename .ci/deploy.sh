curl --fail -XPOST -H "Content-type: application/json" -d '{"name":"'$CI_COMMIT_REF_NAME'"}' 'https://demogitlab:'$FEATURE_MANAGER_TOKEN'@'$FEATURE_MANAGER_DOMAIN'/lxd/container'
echo ""
sed -i "s#image#$CI_REGISTRY_IMAGE/$MODULE$CI_COMMIT_REF_NAME:latest#g" $CI_PROJECT_DIR/.ci/deploy-docker.sh
echo ""
curl --fail -F "artifact=@$CI_PROJECT_DIR/.ci/deploy-docker.sh" -F "container_name=$CI_COMMIT_REF_NAME" "https://demogitlab:"$FEATURE_MANAGER_TOKEN"@"$FEATURE_MANAGER_DOMAIN"/lxd/container/upload_stream"
echo ""
