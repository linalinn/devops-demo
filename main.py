#!/bin/env python3
import http.server
import socket

version = "0.0.1"


class SimpleWebApplication(http.server.BaseHTTPRequestHandler):

    def compose(self, method, url, header, body, ip=None):

        msg = "SuperNew Feature\n"
        msg += "[" + method + "]:\n"
        msg += str(url) + "\n"
        msg += "[HEADERS]:\n" + str(header) + "\n"
        msg += "[BODY]:\n" + str(body) + "\n"
        msg += "[Client IP]:\n" + str(ip[0]) + "\n"
        return (msg)

    def simpleResponse(self, Response):
        self.wfile.write(str(Response).encode(encoding='utf_8', errors='strict'))

    def do_HEAD(self, Content_type="text/plain", status=200):
        self.send_response(status)
        self.send_header("Content-type", Content_type)
        self.end_headers()

    def do_GET(self):
        get_data = "No GET Body"
        self.do_HEAD()
        response = self.compose("GET", self.path, self.headers, get_data, self.client_address)
        self.simpleResponse(response)


class HTTPServerV6(http.server.HTTPServer):
    address_family = socket.AF_INET6


if __name__ == "__main__":
    try:
        server = HTTPServerV6(("::", 80), SimpleWebApplication)
        server.server_version = "Simple Webserver"
        print('Started Webserver...')
        server.serve_forever()
    except KeyboardInterrupt:
        print('shutting down server')
        server.socket.close()